package com.macro.mall.tiny.modules.sys.controller.json;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.macro.mall.tiny.common.api.CommonResult;

import com.macro.mall.tiny.modules.sys.dto.JsonResponse;
import com.macro.mall.tiny.modules.sys.model.SysCustomer;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping("/sys/fastJson")
public class FastJsonController {
    @ApiOperation("得到json")
    @RequestMapping(value = "/getJson", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<String> getJson() {
        List<SysCustomer> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            SysCustomer data = new SysCustomer();
            data.setName("Apple" + i);
            data.setMobilePhone("1398332709" + i);
            data.setLandlinePhone("7456712" + i);

            // 转json
            JSONObject json = (JSONObject) JSON.toJSON(data);
            System.out.println(json.getString("name"));
            System.out.println(json.get("mobilePhone"));

            list.add(data);
        }
        // 这样也可以转json
        String json = JSON.toJSONString(list);
        return CommonResult.success(json);
    }

    @ApiOperation("解析json")
    @RequestMapping(value = "/json", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<SysCustomer> json() {
        String str = "{\"id\": 1,\"name\": \"集团名称\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}";
        SysCustomer data = JSON.parseObject(str, SysCustomer.class);
        System.out.println(data.getName());
        return CommonResult.success(data);
    }

    @ApiOperation("解析json")
    @RequestMapping(value = "/jsonHashMap", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<HashMap<String, Object>> jsonHashMap() {
        String str = "{\"id\": 1,\"name\": \"集团名称\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}";
        HashMap<String, Object> data = JSON.parseObject(str, HashMap.class);
        System.out.println(data.get("name"));
        return CommonResult.success(data);
    }

    @ApiOperation("解析json")
    @RequestMapping(value = "/jsonJSONObject", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<JSONObject> jsonJSONObject() {
        String str = "{\"id\": 1,\"name\": \"集团名称\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}";
        JSONObject parse = (JSONObject) JSON.parse(str);
        String name = (String) parse.get("name");
        System.out.println(name);
        String mobilePhone = parse.getString("mobilePhone");
        System.out.println(mobilePhone);
        return CommonResult.success(parse);
    }

    @ApiOperation("解析不规则json")
    @RequestMapping(value = "/json1", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<JsonResponse> json1() {
        String str = "{\"aa\":\"aaaaaa\",\"baseTime\": \"2023-03-31T03:31:26.000+00:00\",\"name\":\"刘晏麟\",\"myName\":\"刘晏麟\",\"youName\":\"张三\",\"class\":\"测试关键字\",\"sysCustomerList\":[{\"id\": 1,\"name\": \"集团名称1\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"},{\"id\": 2,\"name\": \"集团名称2\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}]}";
        JsonResponse data = JSON.parseObject(str, JsonResponse.class);
        System.out.println(data.getCla());

        return CommonResult.success(data);
    }


    @ApiOperation("解析json")
    @RequestMapping(value = "/jsonList", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<List<SysCustomer>> jsonList() {
        String str = "[{\"id\": 1,\"name\": \"集团名称1\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"},{\"id\": 2,\"name\": \"集团名称2\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}]";
        List<SysCustomer> list = JSON.parseArray(str, SysCustomer.class);
        for (SysCustomer data : list) {
            System.out.println(data.getName());
        }
        return CommonResult.success(list);
    }

    @ApiOperation("解析json")
    @RequestMapping(value = "/jsonListHashMap", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<List<HashMap>> jsonListHashMap() {
        String str = "[{\"id\": 1,\"name\": \"集团名称1\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"},{\"id\": 2,\"name\": \"集团名称2\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}]";
        List<HashMap> list = JSON.parseArray(str, HashMap.class);
        for (HashMap data : list) {
            System.out.println(data.get("name"));
        }
        return CommonResult.success(list);
    }


    @ApiOperation("解析json")
    @RequestMapping(value = "/jsonListJSONArray", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<JSONArray> jsonListJSONArray() {
        String str = "[{\"id\": 1,\"name\": \"集团名称1\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"},{\"id\": 2,\"name\": \"集团名称2\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}]";
        JSONArray parseArray = JSON.parseArray(str);
        for (Object data : parseArray) {
            SysCustomer bean = JSON.parseObject(data.toString(), SysCustomer.class);
            System.out.println(bean.getName());
        }

        for (Object data : parseArray) {
            JSONObject parse = (JSONObject) JSON.parse(data.toString());
            String name = (String) parse.get("name");
            System.out.println(name);
            String mobilePhone = parse.getString("mobilePhone");
            System.out.println(mobilePhone);
        }

        return CommonResult.success(parseArray);
    }

    @ApiOperation("文件")
    @RequestMapping(value = "/jsonFile", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<String> jsonFile() {
        BufferedReader reader = null;
        StringBuilder message = new StringBuilder();
        try {
            FileInputStream fileInputStream = new FileInputStream("F:\\project\\java\\springboot\\mall-tiny\\src\\main\\java\\com\\macro\\mall\\uploads\\fastjson.json");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
            reader = new BufferedReader(inputStreamReader);
            String tempString;
            while ((tempString = reader.readLine()) != null) {
                message.append(tempString);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        return CommonResult.success(message.toString());
    }
}
