package com.macro.mall.tiny.modules.sys.controller.json;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.macro.mall.tiny.common.api.CommonResult;
import com.macro.mall.tiny.modules.sys.dto.JsonResponse;
import com.macro.mall.tiny.modules.sys.model.SysCustomer;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/sys/jackson")
public class JacksonController {
    @ApiOperation("得到json")
    @RequestMapping(value = "/getJson", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<String> getJson() throws IOException {
        List<SysCustomer> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            SysCustomer data = new SysCustomer();
            data.setName("Apple" + i);
            data.setMobilePhone("1398332709" + i);
            data.setLandlinePhone("7456712" + i);
            list.add(data);
        }
        //2.创建Jackson对象 ObjectMapper
        ObjectMapper mapper = new ObjectMapper();
        //3.转换为JSOn
        String json = mapper.writeValueAsString(list);

        //此方法空字段不会被排除
        return CommonResult.success(json);
    }

    @ApiOperation("得到json")
    @RequestMapping(value = "/jsonFile", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<SysCustomer> jsonFile() throws IOException {
        List<SysCustomer> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            SysCustomer data = new SysCustomer();
            data.setName("Apple" + i);
            data.setMobilePhone("1398332709" + i);
            data.setLandlinePhone("7456712" + i);
            list.add(data);
        }
        //2.创建Jackson对象 ObjectMapper
        ObjectMapper mapper = new ObjectMapper();


        mapper.writeValue(new File("./src/main/java/com/macro/mall/uploads/b.json"), list);//写入文件
        mapper.writeValue(new FileWriter("./src/main/java/com/macro/mall/uploads/c.json"), list);//写入文件
        SysCustomer data = mapper.readValue(new File("./src/main/java/com/macro/mall/uploads/fastjson.json"), SysCustomer.class);//从文件中读取
        return CommonResult.success(data);
    }

    /**
     * 解析json
     */
    @ApiOperation("解析json")
    @RequestMapping(value = "/json", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<SysCustomer> json() throws JsonProcessingException {
        String str = "{\"id\": 1,\"name\": \"集团名称1\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}";
        //Jackson核心对象
        ObjectMapper mapper = new ObjectMapper();
        //使用readValue方法进行转换
        SysCustomer data = mapper.readValue(str, SysCustomer.class);

        return CommonResult.success(data);
    }

    /**
     * 解析json
     */
    @ApiOperation("解析json")
    @RequestMapping(value = "/jsonMap", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Map> jsonMap() throws JsonProcessingException {
        String str = "{\"id\": 1,\"name\": \"集团名称\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}";
        //Jackson核心对象
        ObjectMapper mapper = new ObjectMapper();
        //使用readValue方法进行转换
        Map data = mapper.readValue(str, Map.class);
        System.out.println(data.get("name"));

        return CommonResult.success(data);
    }


    @ApiOperation("解析不规则json")
    @RequestMapping(value = "/json1", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<JsonResponse> json1() throws JsonProcessingException {
        String str = "{\"aa\":\"aaaaaa\",\"baseTime\": \"2023-03-31T03:31:26.000+00:00\",\"name\":\"刘晏麟\",\"myName\":\"刘晏麟\",\"youName\":\"张三\",\"class\":\"测试关键字\",\"sysCustomerList\":[{\"id\": 1,\"name\": \"集团名称1\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"},{\"id\": 2,\"name\": \"集团名称2\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}]}";
        //Jackson核心对象
        ObjectMapper mapper = new ObjectMapper();

        // 添加此配置，解决返回的json字符串中的某个键值对无法在实体类中找到对应属性的问题
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        //使用readValue方法进行转换
        JsonResponse data = mapper.readValue(str, JsonResponse.class);

        System.out.println(data.getCla());
        return CommonResult.success(data);
    }

    /**
     * 解析jsonList
     */
    @ApiOperation("解析jsonList")
    @RequestMapping(value = "/jsonList", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<List<SysCustomer>> jsonList() throws JsonProcessingException {
        String str = "[{\"id\": 1,\"name\": \"集团名称1\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"},{\"id\": 2,\"name\": \"集团名称2\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}]";
        //Jackson核心对象
        ObjectMapper mapper = new ObjectMapper();
        //使用readValue方法进行转换
        List<SysCustomer> list = mapper.readValue(str, new TypeReference<List<SysCustomer>>(){});
        return CommonResult.success(list);
    }

    /**
     * 解析jsonList
     */
    @ApiOperation("解析jsonList")
    @RequestMapping(value = "/jsonListMap", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<List<HashMap>> jsonListMap() throws JsonProcessingException {
        String str = "[{\"id\": 1,\"name\": \"集团名称1\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"},{\"id\": 2,\"name\": \"集团名称2\",\"mobilePhone\": \"13983327099\",\"landlinePhone\": \"74587126\",\"area\": \"重庆市江北区\",\"address\": \"昭阳路128号\",\"email\": \"1301969706@qq.com\",\"beginTimes\": \"2023-03-28T16:00:00.000+00:00\",\"endTimes\": \"2023-04-28T16:00:00.000+00:00\",\"status\": 10,\"createAt\": \"2023-03-31T03:31:26.000+00:00\",\"updateAt\": \"2023-04-06T09:59:34.000+00:00\",\"deleteAt\": \"2023-04-06T09:59:34.000+00:00\"}]";
        //Jackson核心对象
        ObjectMapper mapper = new ObjectMapper();
        //使用readValue方法进行转换
        List<HashMap> list = mapper.readValue(str, new TypeReference<List<HashMap>>(){});
        return CommonResult.success(list);
    }


}







