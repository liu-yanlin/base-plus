package com.macro.mall.tiny.modules.card.mapper;

import com.macro.mall.tiny.modules.card.model.CardBinding;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 卡片绑定管理 Mapper 接口
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
public interface CardBindingMapper extends BaseMapper<CardBinding> {

}
