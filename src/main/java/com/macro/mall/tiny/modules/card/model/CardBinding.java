package com.macro.mall.tiny.modules.card.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 卡片绑定管理
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Getter
@Setter
@TableName("card_binding")
@ApiModel(value = "CardBinding对象", description = "卡片绑定管理")
public class CardBinding implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("卡片序列号")
    private Integer cardNumber;

    @ApiModelProperty("集团客户id")
    private Integer customerId;

    @ApiModelProperty("绑定会员id")
    private Integer userId;

    private Date bindingTime;

    @ApiModelProperty("面额(kg)")
    private Integer faceValue;

    private BigDecimal balance;

    @ApiModelProperty("状态 10未绑定 20已绑定 30取消绑定")
    private Integer status;

    @ApiModelProperty("银行消费券号")
    private String serviceNumber;

    private Date createAt;

    private Date updateAt;

    private Date deleteAt;


}
