package com.macro.mall.tiny.modules.sys.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.macro.mall.tiny.common.api.CommonPage;
import com.macro.mall.tiny.common.api.CommonResult;
import com.macro.mall.tiny.modules.sys.model.SysCustomer;
import com.macro.mall.tiny.modules.sys.service.SysCustomerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 集团客户 前端控制器
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@RestController
@RequestMapping("/sys/sysCustomer")
public class SysCustomerController {
    @Autowired
    private SysCustomerService sysCustomerService;

    @ApiOperation("分页查询客户列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CommonPage<SysCustomer>> getList(SysCustomer req,
                                                         @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Page<SysCustomer> list = sysCustomerService.getList(req, pageSize, pageNum);
        return CommonResult.success(CommonPage.restPage(list));
    }

    @ApiOperation("查询所有客户列表")
    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<List<SysCustomer>> getListAll(SysCustomer req) {
        List<SysCustomer> list = sysCustomerService.getListAll(req);
        return CommonResult.success(list);
    }

    @ApiOperation("查询所有客户列表")
    @RequestMapping(value = "/getListAllMap", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<List<Map<String, Object>>> getListAllMap(SysCustomer req) {
        List<Map<String, Object>> list = sysCustomerService.getListAllMap(req);
        return CommonResult.success(list);
    }

    @ApiOperation("查询所有客户列表")
    @RequestMapping(value = "/getlistAllByMap", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<List<SysCustomer>> getlistAllByMap(SysCustomer req) {
        List<SysCustomer> list = sysCustomerService.getlistAllByMap(req);
        return CommonResult.success(list);
    }

    @ApiOperation("查询所有客户数量")
    @RequestMapping(value = "/listCount", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<Long> getListCount(SysCustomer req) {
        Long data = sysCustomerService.getListCount(req);
        return CommonResult.success(data);
    }

    @ApiOperation("根据id获取单条")
    @RequestMapping(value = "/getOneById", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<SysCustomer> getOneById(@RequestParam(value = "id") Long id) {
        SysCustomer data = sysCustomerService.getOneById(id);
        return CommonResult.success(data);
    }

    @ApiOperation("根据ids获取多条")
    @RequestMapping(value = "/getListByIds", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<List<SysCustomer>> getListAll(@RequestParam(value = "ids") String ids) {
        List<SysCustomer> list = sysCustomerService.getListByIds(ids);
        return CommonResult.success(list);
    }

    @ApiOperation("根据条件获取单条")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<SysCustomer> getOne(SysCustomer req) {
        SysCustomer data = sysCustomerService.getOne(req);
        return CommonResult.success(data);
    }


    @ApiOperation("根据条件获取单条")
    @RequestMapping(value = "/getOneMap", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<Map<String, Object>> getOneMap(SysCustomer req) {
        Map<String, Object> data = sysCustomerService.getOneMap(req);
        return CommonResult.success(data);
    }

    @ApiOperation("根据条件获取单条")
    @RequestMapping(value = "/getOneByExists", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<SysCustomer> getOneByExists(SysCustomer req) {
        SysCustomer data = sysCustomerService.getOneByExists(req);
        return CommonResult.success(data);
    }

    @ApiOperation("添加")
    @RequestMapping(value = "/createList", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Boolean> createList(@RequestBody SysCustomer req) {
        boolean data = sysCustomerService.createList(req);
        return CommonResult.success(data);
    }

    @ApiOperation("有则改之无则添加")
    @RequestMapping(value = "/createOrUpdateList", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Boolean> createOrUpdateList(@RequestBody List<SysCustomer> req) {
        boolean data = sysCustomerService.createOrUpdateList(req);
        return CommonResult.success(data);
    }


    @ApiOperation("删除")
    @RequestMapping(value = "/removeByIds", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<Boolean> removeByIds(@RequestParam(value = "ids") String ids) {
        boolean data = sysCustomerService.removeByIds(ids);
        return CommonResult.success(data);
    }

    @ApiOperation("删除")
    @RequestMapping(value = "/removeBy", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Boolean> removeBy(@RequestBody SysCustomer req) {
        boolean data = sysCustomerService.removeBy(req);
        return CommonResult.success(data);
    }

    @ApiOperation("修改")
    @RequestMapping(value = "/updateByIds", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Boolean> updateByIds(@RequestBody List<SysCustomer> req) {
        boolean data = sysCustomerService.UpdateByIds(req);
        return CommonResult.success(data);
    }

    @ApiOperation("修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Boolean> update(@RequestBody SysCustomer req) {
        boolean data = sysCustomerService.Update(req);
        return CommonResult.success(data);
    }

    @ApiOperation("链式查询")
    @RequestMapping(value = "/useChain", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<SysCustomer> useChain() {
        SysCustomer data = sysCustomerService.useChain();
        return CommonResult.success(data);
    }


}

