package com.macro.mall.tiny.modules.sys.controller.upload;

import com.macro.mall.tiny.common.api.CommonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Locale;
import java.util.UUID;

/**
 * <p>
 * 设备表 前端控制器
 * </p>
 *
 * @author macro
 * @since 2022-09-27
 */
@RestController
@RequestMapping("/sys/upload")
public class UploadController {
    /**
     * 上传方式一，从请求体中获取文件信息
     *
     * @param request
     * @return
     * @throws IOException
     */
    @ApiOperation("上传文件")
    @RequestMapping(value = "/upload1", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<String> upload1(HttpServletRequest request) throws IOException {
        MultipartFile file = ((StandardMultipartHttpServletRequest) request).getFile("file");
        String fileName = file.getOriginalFilename();
        String newName = UUID.randomUUID().toString() + fileName.substring(fileName.indexOf("."));

        File projectPath = new File(ResourceUtils.getURL("classpath:").getPath());
        String path = projectPath.getParentFile().toString().replace("target", "");
        // 获取当前操作系统
        String osName = System.getProperties().get("os.name").toString().toLowerCase(Locale.ROOT);
        if (osName.startsWith("win")) {
            path += "src\\main\\java\\com\\macro\\mall\\uploads\\";
        } else {
            path = "/mnt/test";
        }

        File saveFile = new File(path + newName);
        if (!saveFile.getParentFile().exists()) {
            saveFile.getParentFile().mkdirs();
        }
        file.transferTo(saveFile);
        return CommonResult.success(saveFile.getPath());
    }


    /**
     * 上传方式2 MultipartFile 上传
     *
     * @param file
     * @return
     * @throws IOException
     */
    @ApiOperation("上传文件")
    @RequestMapping(value = "/upload2", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<String> upload2(@RequestParam("file") MultipartFile file) throws IOException {//MultipartFile 接收前端传过来的文件
        String fileName = file.getOriginalFilename();
        String newName = UUID.randomUUID().toString() + fileName.substring(fileName.indexOf("."));

        File projectPath = new File(ResourceUtils.getURL("classpath:").getPath());
        String path = projectPath.getParentFile().toString().replace("target", "");
        // 获取当前操作系统
        String osName = System.getProperties().get("os.name").toString().toLowerCase(Locale.ROOT);
        if (osName.startsWith("win")) {
            path += "src\\main\\java\\com\\macro\\mall\\uploads\\";
        } else {
            path = "/mnt/test";
        }
        File saveFile = new File(path + newName);
        if (!saveFile.getParentFile().exists()) {
            saveFile.getParentFile().mkdirs();
        }
        file.transferTo(saveFile);
        return CommonResult.success(saveFile.getPath());
    }

    // String data = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAFJklEQVR4Xu3VsRGAQAwEsaf/oqEBINj0RO7A8u9w3efcx0eAwKvAJRAvg8C3gEC8DgI/AgLxPAgIxBsg0AT8QZqbqREBgYwc2ppNQCDNzdSIgEBGDm3NJiCQ5mZqREAgI4e2ZhMQSHMzNSIgkJFDW7MJCKS5mRoREMjIoa3ZBATS3EyNCAhk5NDWbAICaW6mRgQEMnJoazYBgTQ3UyMCAhk5tDWbgECam6kRAYGMHNqaTUAgzc3UiIBARg5tzSYgkOZmakRAICOHtmYTEEhzMzUiIJCRQ1uzCQikuZkaERDIyKGt2QQE0txMjQgIZOTQ1mwCAmlupkYEBDJyaGs2AYE0N1MjAgIZObQ1m4BAmpupEQGBjBzamk1AIM3N1IiAQEYObc0mIJDmZmpEQCAjh7ZmExBIczM1IiCQkUNbswkIpLmZGhEQyMihrdkEBNLcTI0ICGTk0NZsAgJpbqZGBAQycmhrNgGBNDdTIwICGTm0NZuAQJqbqREBgYwc2ppNQCDNzdSIgEBGDm3NJiCQ5mZqREAgI4e2ZhMQSHMzNSIgkJFDW7MJCKS5mRoREMjIoa3ZBATS3EyNCAhk5NDWbAICaW6mRgQEMnJoazYBgTQ3UyMCAhk5tDWbgECam6kRAYGMHNqaTUAgzc3UiIBARg5tzSYgkOZmakRAICOHtmYTEEhzMzUiIJCRQ1uzCQikuZkaERDIyKGt2QQE0txMjQgIZOTQ1mwCAmlupkYEBDJyaGs2AYE0N1MjAgIZObQ1m4BAmpupEQGBjBzamk1AIM3N1IiAQEYObc0mIJDmZmpEQCAjh7ZmExBIczM1IiCQkUNbswkIpLmZGhEQyMihrdkEBNLcTI0ICGTk0NZsAgJpbqZGBAQycmhrNgGBNDdTIwICGTm0NZuAQJqbqREBgYwc2ppNQCDNzdSIgEBGDm3NJiCQ5mZqREAgI4e2ZhMQSHMzNSIgkJFDW7MJCKS5mRoREMjIoa3ZBATS3EyNCAhk5NDWbAICaW6mRgQEMnJoazYBgTQ3UyMCAhk5tDWbgECam6kRAYGMHNqaTUAgzc3UiIBARg5tzSYgkOZmakRAICOHtmYTEEhzMzUiIJCRQ1uzCQikuZkaERDIyKGt2QQE0txMjQgIZOTQ1mwCAmlupkYEBDJyaGs2AYE0N1MjAgIZObQ1m4BAmpupEQGBjBzamk1AIM3N1IiAQEYObc0mIJDmZmpEQCAjh7ZmExBIczM1IiCQkUNbswkIpLmZGhEQyMihrdkEBNLcTI0ICGTk0NZsAgJpbqZGBAQycmhrNgGBNDdTIwICGTm0NZuAQJqbqREBgYwc2ppNQCDNzdSIgEBGDm3NJiCQ5mZqREAgI4e2ZhMQSHMzNSIgkJFDW7MJCKS5mRoREMjIoa3ZBATS3EyNCAhk5NDWbAICaW6mRgQEMnJoazYBgTQ3UyMCAhk5tDWbgECam6kRAYGMHNqaTUAgzc3UiIBARg5tzSYgkOZmakRAICOHtmYTEEhzMzUiIJCRQ1uzCQikuZkaERDIyKGt2QQE0txMjQgIZOTQ1mwCAmlupkYEBDJyaGs2AYE0N1MjAgIZObQ1m4BAmpupEQGBjBzamk1AIM3N1IiAQEYObc0mIJDmZmpEQCAjh7ZmExBIczM1IiCQkUNbswkIpLmZGhEQyMihrdkEBNLcTI0ICGTk0NZsAgJpbqZGBAQycmhrNgGBNDdTIwICGTm0NZuAQJqbqREBgYwc2ppNQCDNzdSIwAOn4Y9IyHT+ZAAAAABJRU5ErkJggg==";
    @ApiOperation("上传文件")
    @RequestMapping(value = "/upload3", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<String> upload3(@RequestParam("base64") String base64) throws FileNotFoundException {
        String base64Data = base64.split(",")[1];
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] bytes = decoder.decode(base64Data);
        // 获取当前操作系统
        String osName = System.getProperties().get("os.name").toString().toLowerCase(Locale.ROOT);
        //String path = "";// 只用相对路劲也可以
        File projectPath = new File(ResourceUtils.getURL("classpath:").getPath());
        String path = projectPath.getParentFile().toString().replace("target", "");

        if (osName.startsWith("win")) {
            path += "src\\main\\java\\com\\macro\\mall\\uploads\\";
        } else {
            path = "/mnt/test";
        }
        String newName = UUID.randomUUID().toString() + ".png";
        File saveFile = new File(path + newName);
        if (!saveFile.getParentFile().exists()) {
            saveFile.getParentFile().mkdirs();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(saveFile);
            fos.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return CommonResult.success(saveFile.getPath());
    }

}