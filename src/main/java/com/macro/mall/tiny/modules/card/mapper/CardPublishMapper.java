package com.macro.mall.tiny.modules.card.mapper;

import com.macro.mall.tiny.modules.card.model.CardPublish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 卡片发行记录 Mapper 接口
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
public interface CardPublishMapper extends BaseMapper<CardPublish> {

}
