package com.macro.mall.tiny.modules.card.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 卡片绑定管理 前端控制器
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@RestController
@RequestMapping("/card/cardBinding")
public class CardBindingController {

}

