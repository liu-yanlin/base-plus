package com.macro.mall.tiny.modules.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.macro.mall.tiny.modules.sys.model.SysCustomer;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 集团客户 服务类
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
public interface SysCustomerService extends IService<SysCustomer> {
    /**
     * 分页查询
     */
    Page<SysCustomer> getList(SysCustomer req, Integer pageSize, Integer pageNum);

    /**
     * 查询所有
     */
    List<SysCustomer> getListAll(SysCustomer req);

    /**
     * 查询所有,返回map格式
     */
    List<Map<String, Object>> getListAllMap(SysCustomer req);

    /**
     * 查询所有,条件为map格式
     */
    List<SysCustomer> getlistAllByMap(SysCustomer req);

    /**
     * 获取条数
     */
    Long getListCount(SysCustomer req);

    /**
     * 根据id获取单条
     */
    SysCustomer getOneById(Long id);

    /**
     * 根据ids获取多条
     */
    List<SysCustomer> getListByIds(String ids);

    /**
     * 根据条件获取单条
     */
    SysCustomer getOne(SysCustomer req);

    /**
     * 根据条件获取单条 map
     */
    Map<String, Object> getOneMap(SysCustomer req);

    /**
     * 根据条件获取单条 Exists
     */
    SysCustomer getOneByExists(SysCustomer req);

    /**
     * 创建，单、多
     */
    boolean createList(SysCustomer req);

    /**
     * 有则改之无则添加 单、多
     */
    boolean createOrUpdateList(List<SysCustomer> req);

    /**
     * 根据id删除 单、多
     */
    boolean removeByIds(String req);

    /**
     * 根据条件删除 单、多
     */
    boolean removeBy(SysCustomer req);

    /**
     * 根据id修改 单、多
     */
    boolean UpdateByIds(List<SysCustomer> req);

    /**
     * 根据条件修改 单、多
     */
    boolean Update(SysCustomer req);


    /**
     * 链式查询
     */
    SysCustomer useChain();


}
