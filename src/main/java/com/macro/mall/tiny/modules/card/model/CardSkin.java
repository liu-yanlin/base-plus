package com.macro.mall.tiny.modules.card.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 卡面管理(皮肤)
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Getter
@Setter
@TableName("card_skin")
@ApiModel(value = "CardSkin对象", description = "卡面管理(皮肤)")
public class CardSkin implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("面额(kg)")
    private Integer faceValue;

    @ApiModelProperty("颜色")
    private String colour;

    @ApiModelProperty("集团客户id")
    private Integer customerId;

    @ApiModelProperty("类型 10实体卡片 20数字卡密")
    private Integer type;

    @ApiModelProperty("状态 10启用 20停用")
    private Integer status;

    @ApiModelProperty("略缩图")
    private String image;

    @ApiModelProperty("创建人id")
    private Integer adminId;

    private Date createAt;

    private Date updateAt;

    private Date deleteAt;

    @TableField(exist = false)
    @ApiModelProperty("集团客户名称")
    private String customerName;

    @TableField(exist = false)
    @ApiModelProperty("总面额(kg)")
    private Integer faceValueSum;

    @TableField(exist = false)
    @ApiModelProperty("集团客户创建时间")
    private String customerCreateAt;

}
