package com.macro.mall.tiny.modules.card.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.yulichang.query.MPJQueryWrapper;
import com.macro.mall.tiny.modules.card.mapper.CardSkinMapper;
import com.macro.mall.tiny.modules.card.model.CardSkin;
import com.macro.mall.tiny.modules.card.service.CardSkinService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.List;


/**
 * <p>
 * 卡面管理(皮肤) 服务实现类
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Service
public class CardSkinServiceImpl extends ServiceImpl<CardSkinMapper, CardSkin> implements CardSkinService {
    private static final Logger log = LoggerFactory.getLogger(CardSkinServiceImpl.class);

    @Autowired
    private CardSkinMapper cardSkinMapper;

    @Override
    public CardSkin getOne(Long id) {
        MPJQueryWrapper<CardSkin> wrapper = new MPJQueryWrapper<>();

        wrapper.setAlias("card_skin");//设置表别名,不设置的话默认为t
        log.warn("表别名:{}", wrapper.getAlias());//获取表别名

        wrapper.selectAll(CardSkin.class).select("sys_customer.name as customerName,sys_customer.create_at as customer_create_at");

        wrapper.leftJoin("sys_customer on sys_customer.id = card_skin.customer_id");

        wrapper.eq("card_skin.id", id);

        CardSkin data = getOne(wrapper, false);
        SimpleDateFormat sdf  =  new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(data.getCreateAt());
        log.warn(date);
        log.warn(data.getCustomerCreateAt());

        log.warn("关联查询单条:{}", JSONUtil.parse(data));
        return data;
    }

    @Override
    public Page<CardSkin> getList(CardSkin req, Integer pageSize, Integer pageNum) {
        MPJQueryWrapper<CardSkin> wrapper = new MPJQueryWrapper<>();

        wrapper.selectAll(CardSkin.class).select("sys_customer.name as customerName");

        wrapper.leftJoin("sys_customer on t.customer_id = sys_customer.id");

        if (StringUtils.hasLength(req.getName())) {
            wrapper.like("t.name", req.getName());
        }
        if (StringUtils.hasLength(req.getCustomerName())) {
            wrapper.like("sys_customer.name", req.getCustomerName());
        }

        Page<CardSkin> page = new Page<>(pageNum, pageSize);

        return page(page, wrapper);
    }

    @Override
    public List<CardSkin> getList1(CardSkin req, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        return cardSkinMapper.getCardSkinList(req);
    }

    @Override
    public CardSkin getOne1(CardSkin req) {
        /*//方案一
        QueryWrapper<CardSkin> wrapper = new QueryWrapper<>();
        wrapper.select("customer_id,sum(face_value) as faceValueSum");
        if (req.getCustomerId() > 0) {
            wrapper.eq("customer_id", req.getCustomerId());
        }*/


        //方案二
        MPJQueryWrapper<CardSkin> wrapper = new MPJQueryWrapper<>();
        wrapper.select("t.customer_id").select("sum(t.face_value) as faceValueSum");
        if (req.getCustomerId() > 0) {
            wrapper.eq("t.customer_id", req.getCustomerId());
        }

        // 注意 如果记录有多条会报错,第二个参数为true就报错  为false不报错
        return getOne(wrapper, false);
    }


}
