package com.macro.mall.tiny.modules.sys.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.macro.mall.tiny.modules.sys.model.SysCustomer;
import com.macro.mall.tiny.modules.sys.mapper.SysCustomerMapper;
import com.macro.mall.tiny.modules.sys.service.SysCustomerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * <p>
 * 集团客户 服务实现类
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Service
public class SysCustomerServiceImpl extends ServiceImpl<SysCustomerMapper, SysCustomer> implements SysCustomerService {
    private static final Logger log = LoggerFactory.getLogger(SysCustomerServiceImpl.class);

    @Override
    public Page<SysCustomer> getList(SysCustomer req, Integer pageSize, Integer pageNum) {
        Page<SysCustomer> page = new Page<>(pageNum, pageSize);
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        LambdaQueryWrapper<SysCustomer> lambda = wrapper.lambda();

        //wrapper.lambda().like(SysCustomer::getName, req.getName());

        if (StringUtils.hasLength(req.getName())) {
            lambda.like(SysCustomer::getName, req.getName());
        }

        return page(page, wrapper);
    }

    @Override
    public List<SysCustomer> getListAll(SysCustomer req) {
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        wrapper.select("id,name");
        if (StringUtils.hasLength(req.getName())) {
            wrapper.like("name", req.getName());
        }
        return list(wrapper);
    }

    @Override
    public List<Map<String, Object>> getListAllMap(SysCustomer req) {
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        wrapper.select("id,name");
        if (StringUtils.hasLength(req.getName())) {
            wrapper.like("name", req.getName());
        }
        List<Map<String, Object>> list = listMaps(wrapper);
        log.warn("获取所有,返回map格式:{}", list);
        return list;
    }


    @Override
    public List<SysCustomer> getlistAllByMap(SysCustomer req) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", req.getName());
        List<SysCustomer> list = listByMap(map);
        log.warn("根据Map条件查询:{}", JSONUtil.parse(list));
        return list;
    }

    @Override
    public Long getListCount(SysCustomer req) {
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        if (StringUtils.hasLength(req.getName())) {
            wrapper.like("name", req.getName());
        }
        return count(wrapper);
    }

    @Override
    public SysCustomer getOneById(Long id) {
        SysCustomer dataById = getById(id);
        log.warn("根据id获取单条:{}", JSONUtil.parse(dataById));
        return dataById;
    }

    @Override
    public List<SysCustomer> getListByIds(String req) {
        String[] ids = req.split(",");
        List<SysCustomer> list = listByIds(Arrays.asList(ids));
        log.warn("根据ids查询:{}", JSONUtil.parse(list));
        return list;
    }

    @Override
    public SysCustomer getOne(SysCustomer req) {
        //TODO 记录存在多条会报错
        // 注意 如果记录有多条会报错,第二个参数为true就报错  为false不报错
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        wrapper.lambda().
                select(SysCustomer::getName, SysCustomer::getId).
                like(SysCustomer::getName, req.getName()).
                orderByDesc(SysCustomer::getId);
//        last("limit 1");
        SysCustomer data = getOne(wrapper, false);
        log.warn("根据条件获取单条:{}", JSONUtil.parse(data));

        return data;
    }


    @Override
    public Map<String, Object> getOneMap(SysCustomer req) {
        //TODO 记录存在多条会报错
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        wrapper.select("id,name").like("name", "三").last("limit 1");
        Map<String, Object> map = getMap(wrapper);
        log.warn("根据条件获取单条,返回map格式:{}", map);
        return map;
    }

    @Override
    public SysCustomer getOneByExists(SysCustomer req) {
        //TODO 记录存在多条会报错
        // 注意 如果记录有多条会报错,第二个参数为true就报错  为false不报错
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        wrapper.select("id,name").like("name", "三").
                exists("select 1 from card_skin where card_skin.customer_id = sys_customer.id").orderByDesc("id");
        SysCustomer data = getOne(wrapper, false);
        log.warn("复合查询，根据件获取单条:{}", JSONUtil.parse(data));
        return data;
    }


    @Override
    public boolean createList(SysCustomer req) {
        // 插入一条记录
        save(req);
        log.warn("添加后返回id:{}", req.getId());

        req.setId(0);
        List<SysCustomer> list = new ArrayList<>();
        list.add(req);
        list.add(req);

        // 插入多条记录
        saveBatch(list);

        return true;
    }

    @Override
    public boolean createOrUpdateList(List<SysCustomer> req) {

        // 有则改之无则添加 一条记录
        saveOrUpdate(req.get(0));

        // 有则改之无则添加 一条记录
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(SysCustomer::getId, req.get(0).getId());
        saveOrUpdate(req.get(0), wrapper);

        List<SysCustomer> list = new ArrayList<>();
        list.add(req.get(1));
        list.add(req.get(2));

        // 有则改之无则添加 多条记录
        saveOrUpdateBatch(list);
        return true;
    }

    @Override
    public boolean removeByIds(String req) {
        String[] ids = req.split(",");
        // 删除一条记录
        removeById(ids[0]);

        // 删除多条记录
        removeByIds(Arrays.asList(ids));

        return true;
    }

    @Override
    public boolean removeBy(SysCustomer req) {
        // 根据 entity 条件，删除记录
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        wrapper.lambda().like(SysCustomer::getName, req.getName());
        remove(wrapper);


        // 根据 columnMap 条件，删除记录
        Map<String, Object> map = new HashMap<>();
        map.put("id", req.getId());
        removeByMap(map);

        return true;
    }

    @Override
    public boolean UpdateByIds(List<SysCustomer> req) {
        // 根据 ID 选择修改
        updateById(req.get(0));

        // 根据ID 批量更新
        List<SysCustomer> list = new ArrayList<>();
        list.add(req.get(1));
        list.add(req.get(2));

        updateBatchById(list);

        return true;
    }

    @Override
    public boolean Update(SysCustomer req) {
        // 更新类容
//        SysCustomer data = new SysCustomer();
//        data.setName("根据条件更新");
//        data.setUpdateAt(new Date());
        // 查询条件
        QueryWrapper<SysCustomer> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(SysCustomer::getName, req.getName());
        update(req, wrapper);
        return true;
    }


    @Override
    public SysCustomer useChain() {
        //TODO 记录存在多条会报错
        SysCustomer data = query().eq("id", 1).last("limit 1").one();
        log.warn("1链式查询:{}", JSONUtil.parse(data));


        List<SysCustomer> list = lambdaQuery().eq(SysCustomer::getName, "王五").list();
        log.warn("2链式查询:{}", JSONUtil.parse(list));


        update().eq("id", 4).remove();
        lambdaUpdate().eq(SysCustomer::getId, 5).remove();


        SysCustomer update = new SysCustomer();
        update.setName("链式跟新");
        update().eq("id", 6).update(update);
        lambdaUpdate().eq(SysCustomer::getId, 7).update(update);


        return data;
    }


}
