package com.macro.mall.tiny.modules.card.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 卡片发行记录
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Getter
@Setter
@TableName("card_publish")
@ApiModel(value = "CardPublish对象", description = "卡片发行记录")
public class CardPublish implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Date publishTime;

    @ApiModelProperty("操作人id")
    private Integer adminId;

    @ApiModelProperty("集团客户id")
    private Integer customerId;

    @ApiModelProperty("卡面编号id")
    private Integer skinId;

    @ApiModelProperty("数量")
    private Integer count;

    @ApiModelProperty("起始卡片序列号")
    private Integer beginNumber;

    @ApiModelProperty("截止卡片序列号")
    private Integer endNumber;

    @ApiModelProperty("状态 10添加库存 20确认发行 30取消发行 40批次停用")
    private Integer status;

    @ApiModelProperty("类型 10预存费用型 20用户充值型")
    private Integer type;

    private String serviceNumber;

    private String serviceOrderNumber;

    private BigDecimal serviceMoney;

    private Date createAt;

    private Date updateAt;

    private Date deleteAt;


}
