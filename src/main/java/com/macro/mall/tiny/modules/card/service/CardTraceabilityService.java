package com.macro.mall.tiny.modules.card.service;

import com.macro.mall.tiny.modules.card.model.CardTraceability;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 卡片溯源记录 服务类
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
public interface CardTraceabilityService extends IService<CardTraceability> {

}
