package com.macro.mall.tiny.modules.card.service.impl;

import com.macro.mall.tiny.modules.card.model.CardPublish;
import com.macro.mall.tiny.modules.card.mapper.CardPublishMapper;
import com.macro.mall.tiny.modules.card.service.CardPublishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 卡片发行记录 服务实现类
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Service
public class CardPublishServiceImpl extends ServiceImpl<CardPublishMapper, CardPublish> implements CardPublishService {

}
