package com.macro.mall.tiny.modules.card.service.impl;

import com.macro.mall.tiny.modules.card.model.CardTraceability;
import com.macro.mall.tiny.modules.card.mapper.CardTraceabilityMapper;
import com.macro.mall.tiny.modules.card.service.CardTraceabilityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 卡片溯源记录 服务实现类
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Service
public class CardTraceabilityServiceImpl extends ServiceImpl<CardTraceabilityMapper, CardTraceability> implements CardTraceabilityService {

}
