package com.macro.mall.tiny.modules.sys.mapper;

import com.macro.mall.tiny.modules.sys.model.SysCustomer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 集团客户 Mapper 接口
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
public interface SysCustomerMapper extends BaseMapper<SysCustomer> {

}
