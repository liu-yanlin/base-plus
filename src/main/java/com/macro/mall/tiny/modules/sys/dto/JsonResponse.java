package com.macro.mall.tiny.modules.sys.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.macro.mall.tiny.common.exception.CustomDateSerializer;
import com.macro.mall.tiny.modules.sys.model.SysCustomer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class JsonResponse extends SysCustomer {

    @ApiModelProperty("客户列表(测试解析json)")
    private List<SysCustomer> sysCustomerList;

    @ApiModelProperty("测试解析json")
    private String name;

    @ApiModelProperty("测试解析json")
    private String myName;

    @ApiModelProperty("测试解析json")
    @JSONField(serialize = false)//表示此属性不会参与任何的序列化和反序列化 fastjson
    @JsonIgnore//表示此属性不会参与任何的序列化和反序列化 jackson
    private String youName;

    @ApiModelProperty("测试解析json")
    @JSONField(name = "class")//关键词冲突,取别名 fastjson
    @JsonProperty("class")//关键词冲突,取别名 jackson
    private String cla;

//    @JSONField(format="yyyy-MM-dd HH:mm:ss")//fastjson包，但是fastjson不能用
//    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Shanghai")//jackson包，①fastjson能用，格式随意 ②jackson只有固定几种格式，乱写会报错
    @JsonSerialize(using = CustomDateSerializer.class)//jackson包，但是jackson和fastjson都能用
    private Date baseTime;
}
