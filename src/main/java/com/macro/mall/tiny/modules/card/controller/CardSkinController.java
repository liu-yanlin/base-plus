package com.macro.mall.tiny.modules.card.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.macro.mall.tiny.common.api.CommonPage;
import com.macro.mall.tiny.common.api.CommonResult;
import com.macro.mall.tiny.modules.card.model.CardSkin;
import com.macro.mall.tiny.modules.card.service.CardSkinService;
import com.macro.mall.tiny.modules.sys.model.SysCustomer;
import com.macro.mall.tiny.modules.sys.service.SysCustomerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 卡面管理(皮肤) 前端控制器
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@RestController
@RequestMapping("/card/cardSkin")
public class CardSkinController {
    @Autowired
    private CardSkinService cardSkinService;

    @ApiOperation("查询单条")
    @RequestMapping(value = "/getOne", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CardSkin> getOne(@RequestParam(value = "id") Long id) {
        CardSkin data = cardSkinService.getOne(id);
        return CommonResult.success(data);
    }


    @ApiOperation("分页查询")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CommonPage<CardSkin>> getList(CardSkin req,
                                                      @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                                      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Page<CardSkin> list = cardSkinService.getList(req, pageSize, pageNum);
        return CommonResult.success(CommonPage.restPage(list));
    }


    @ApiOperation("分页查询")
    @RequestMapping(value = "/list1", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CommonPage<CardSkin>> getList1(CardSkin req,
                                                      @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                                      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        List<CardSkin> list = cardSkinService.getList1(req, pageSize, pageNum);
        return CommonResult.success(CommonPage.restPage(list));
    }


    @ApiOperation("复合查询")
    @RequestMapping(value = "/getOne1", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CardSkin> getOne1(CardSkin req) {
        CardSkin data = cardSkinService.getOne1(req);
        return CommonResult.success(data);
    }


}

