package com.macro.mall.tiny.modules.card.service.impl;

import com.macro.mall.tiny.modules.card.model.CardBinding;
import com.macro.mall.tiny.modules.card.mapper.CardBindingMapper;
import com.macro.mall.tiny.modules.card.service.CardBindingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 卡片绑定管理 服务实现类
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Service
public class CardBindingServiceImpl extends ServiceImpl<CardBindingMapper, CardBinding> implements CardBindingService {

}
