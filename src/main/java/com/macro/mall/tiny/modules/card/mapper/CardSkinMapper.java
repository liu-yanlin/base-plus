package com.macro.mall.tiny.modules.card.mapper;

import com.macro.mall.tiny.modules.card.model.CardSkin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 卡面管理(皮肤) Mapper 接口
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
public interface CardSkinMapper extends BaseMapper<CardSkin> {
    List<CardSkin> getCardSkinList(@Param("req") CardSkin req);

}
