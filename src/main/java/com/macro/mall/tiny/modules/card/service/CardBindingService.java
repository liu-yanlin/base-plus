package com.macro.mall.tiny.modules.card.service;

import com.macro.mall.tiny.modules.card.model.CardBinding;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 卡片绑定管理 服务类
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
public interface CardBindingService extends IService<CardBinding> {

}
