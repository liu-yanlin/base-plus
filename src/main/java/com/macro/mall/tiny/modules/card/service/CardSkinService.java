package com.macro.mall.tiny.modules.card.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.macro.mall.tiny.modules.card.model.CardSkin;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * <p>
 * 卡面管理(皮肤) 服务类
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
public interface CardSkinService extends IService<CardSkin> {
    /**
     * 关联获取单条
     */
    CardSkin getOne(Long id);

    /**
     * 关联获取列表
     */
    Page<CardSkin> getList(CardSkin req, Integer pageSize, Integer pageNum);

    /**
     * 关联获取列表(mybatis)
     */
    List<CardSkin> getList1(CardSkin req, Integer pageSize, Integer pageNum);

    /**
     * 复合查询
     */
    CardSkin getOne1(CardSkin req);
}

