package com.macro.mall.tiny.modules.card.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 卡片溯源记录
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Getter
@Setter
@TableName("card_traceability")
@ApiModel(value = "CardTraceability对象", description = "卡片溯源记录")
public class CardTraceability implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("卡片序列号")
    private Integer cardNumber;

    @ApiModelProperty("入库操作人id")
    private Integer storageAdminId;

    private Date storageTime;

    @ApiModelProperty("发行操作人id")
    private Integer publishAdminId;

    private Date publishTime;

    @ApiModelProperty("领用销售员id")
    private Integer receiveAdminId;

    private Date receiveTime;

    @ApiModelProperty("绑定会员id")
    private Integer bindingUserId;

    private Date bindingTime;

    private Date writeOffTime;

    @ApiModelProperty("状态 10入库 20发行 30领用 40绑定 50使用 60销毁")
    private Integer status;

    private Date createAt;

    private Date updateAt;

    private Date deleteAt;


}
