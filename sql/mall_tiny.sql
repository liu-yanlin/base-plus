/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : mall_tiny

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2020-08-24 14:06:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ums_admin
-- ----------------------------
DROP TABLE IF EXISTS `ums_admin`;
CREATE TABLE `ums_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `icon` varchar(500) DEFAULT NULL COMMENT '头像',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `nick_name` varchar(200) DEFAULT NULL COMMENT '昵称',
  `note` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` int(1) DEFAULT '1' COMMENT '帐号启用状态：0->禁用；1->启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='后台用户表';

-- ----------------------------
-- Records of ums_admin
-- ----------------------------
INSERT INTO `ums_admin` VALUES ('1', 'test', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/timg.jpg', 'test@qq.com', '测试账号', null, '2018-09-29 13:55:30', '2018-09-29 13:55:39', '1');
INSERT INTO `ums_admin` VALUES ('3', 'admin', '$2a$10$.E1FokumK5GIXWgKlg.Hc.i/0/2.qdAwYFL1zc5QHdyzpXOr38RZO', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/timg.jpg', 'admin@163.com', '系统管理员', '系统管理员', '2018-10-08 13:32:47', '2019-04-20 12:45:16', '1');
INSERT INTO `ums_admin` VALUES ('4', 'macro', '$2a$10$Bx4jZPR7GhEpIQfefDQtVeS58GfT5n6mxs/b4nLLK65eMFa16topa', 'string', 'macro@qq.com', 'macro', 'macro专用', '2019-10-06 15:53:51', '2020-02-03 14:55:55', '1');
INSERT INTO `ums_admin` VALUES ('6', 'productAdmin', '$2a$10$6/.J.p.6Bhn7ic4GfoB5D.pGd7xSiD1a9M6ht6yO0fxzlKJPjRAGm', null, 'product@qq.com', '商品管理员', '只有商品权限', '2020-02-07 16:15:08', null, '1');
INSERT INTO `ums_admin` VALUES ('7', 'orderAdmin', '$2a$10$UqEhA9UZXjHHA3B.L9wNG.6aerrBjC6WHTtbv1FdvYPUI.7lkL6E.', null, 'order@qq.com', '订单管理员', '只有订单管理权限', '2020-02-07 16:15:50', null, '1');
INSERT INTO `ums_admin` VALUES ('10', 'ceshi', '$2a$10$RaaNo9CC0RSms8mc/gJpCuOWndDT4pHH0u5XgZdAAYFs1Uq4sOPRi', null, 'ceshi@qq.com', 'ceshi', null, '2020-03-13 16:23:30', null, '1');

-- ----------------------------
-- Table structure for ums_admin_login_log
-- ----------------------------
DROP TABLE IF EXISTS `ums_admin_login_log`;
CREATE TABLE `ums_admin_login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `user_agent` varchar(100) DEFAULT NULL COMMENT '浏览器登录类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8 COMMENT='后台用户登录日志表';

-- ----------------------------
-- Records of ums_admin_login_log
-- ----------------------------
INSERT INTO `ums_admin_login_log` VALUES ('285', '3', '2020-08-24 14:05:21', '0:0:0:0:0:0:0:1', null, null);
INSERT INTO `ums_admin_login_log` VALUES ('286', '10', '2020-08-24 14:05:39', '0:0:0:0:0:0:0:1', null, null);

-- ----------------------------
-- Table structure for ums_admin_role_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_admin_role_relation`;
CREATE TABLE `ums_admin_role_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='后台用户和角色关系表';

-- ----------------------------
-- Records of ums_admin_role_relation
-- ----------------------------
INSERT INTO `ums_admin_role_relation` VALUES ('26', '3', '5');
INSERT INTO `ums_admin_role_relation` VALUES ('27', '6', '1');
INSERT INTO `ums_admin_role_relation` VALUES ('28', '7', '2');
INSERT INTO `ums_admin_role_relation` VALUES ('29', '1', '5');
INSERT INTO `ums_admin_role_relation` VALUES ('30', '4', '5');
INSERT INTO `ums_admin_role_relation` VALUES ('31', '8', '5');
INSERT INTO `ums_admin_role_relation` VALUES ('34', '12', '6');
INSERT INTO `ums_admin_role_relation` VALUES ('38', '13', '5');
INSERT INTO `ums_admin_role_relation` VALUES ('39', '10', '8');

-- ----------------------------
-- Table structure for ums_menu
-- ----------------------------
DROP TABLE IF EXISTS `ums_menu`;
CREATE TABLE `ums_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父级ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `title` varchar(100) DEFAULT NULL COMMENT '菜单名称',
  `level` int(4) DEFAULT NULL COMMENT '菜单级数',
  `sort` int(4) DEFAULT NULL COMMENT '菜单排序',
  `name` varchar(100) DEFAULT NULL COMMENT '前端名称',
  `icon` varchar(200) DEFAULT NULL COMMENT '前端图标',
  `hidden` int(1) DEFAULT NULL COMMENT '前端隐藏',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';

-- ----------------------------
-- Records of ums_menu
-- ----------------------------
INSERT INTO `ums_menu` VALUES ('1', '0', '2020-02-02 14:50:36', '商品', '0', '0', 'pms', 'product', '1');
INSERT INTO `ums_menu` VALUES ('2', '1', '2020-02-02 14:51:50', '商品列表', '1', '0', 'product', 'product-list', '0');
INSERT INTO `ums_menu` VALUES ('3', '1', '2020-02-02 14:52:44', '添加商品', '1', '0', 'addProduct', 'product-add', '0');
INSERT INTO `ums_menu` VALUES ('4', '1', '2020-02-02 14:53:51', '商品分类', '1', '0', 'productCate', 'product-cate', '0');
INSERT INTO `ums_menu` VALUES ('5', '1', '2020-02-02 14:54:51', '商品类型', '1', '0', 'productAttr', 'product-attr', '0');
INSERT INTO `ums_menu` VALUES ('6', '1', '2020-02-02 14:56:29', '品牌管理', '1', '0', 'brand', 'product-brand', '0');
INSERT INTO `ums_menu` VALUES ('7', '0', '2020-02-02 16:54:07', '订单', '0', '0', 'oms', 'order', '1');
INSERT INTO `ums_menu` VALUES ('8', '7', '2020-02-02 16:55:18', '订单列表', '1', '0', 'order', 'product-list', '0');
INSERT INTO `ums_menu` VALUES ('9', '7', '2020-02-02 16:56:46', '订单设置', '1', '0', 'orderSetting', 'order-setting', '0');
INSERT INTO `ums_menu` VALUES ('10', '7', '2020-02-02 16:57:39', '退货申请处理', '1', '0', 'returnApply', 'order-return', '0');
INSERT INTO `ums_menu` VALUES ('11', '7', '2020-02-02 16:59:40', '退货原因设置', '1', '0', 'returnReason', 'order-return-reason', '0');
INSERT INTO `ums_menu` VALUES ('12', '0', '2020-02-04 16:18:00', '营销', '0', '0', 'sms', 'sms', '1');
INSERT INTO `ums_menu` VALUES ('13', '12', '2020-02-04 16:19:22', '秒杀活动列表', '1', '0', 'flash', 'sms-flash', '0');
INSERT INTO `ums_menu` VALUES ('14', '12', '2020-02-04 16:20:16', '优惠券列表', '1', '0', 'coupon', 'sms-coupon', '0');
INSERT INTO `ums_menu` VALUES ('16', '12', '2020-02-07 16:22:38', '品牌推荐', '1', '0', 'homeBrand', 'product-brand', '0');
INSERT INTO `ums_menu` VALUES ('17', '12', '2020-02-07 16:23:14', '新品推荐', '1', '0', 'homeNew', 'sms-new', '0');
INSERT INTO `ums_menu` VALUES ('18', '12', '2020-02-07 16:26:38', '人气推荐', '1', '0', 'homeHot', 'sms-hot', '0');
INSERT INTO `ums_menu` VALUES ('19', '12', '2020-02-07 16:28:16', '专题推荐', '1', '0', 'homeSubject', 'sms-subject', '0');
INSERT INTO `ums_menu` VALUES ('20', '12', '2020-02-07 16:28:42', '广告列表', '1', '0', 'homeAdvertise', 'sms-ad', '0');
INSERT INTO `ums_menu` VALUES ('21', '0', '2020-02-07 16:29:13', '权限', '0', '0', 'ums', 'ums', '0');
INSERT INTO `ums_menu` VALUES ('22', '21', '2020-02-07 16:29:51', '用户列表', '1', '0', 'admin', 'ums-admin', '0');
INSERT INTO `ums_menu` VALUES ('23', '21', '2020-02-07 16:30:13', '角色列表', '1', '0', 'role', 'ums-role', '0');
INSERT INTO `ums_menu` VALUES ('24', '21', '2020-02-07 16:30:53', '菜单列表', '1', '0', 'menu', 'ums-menu', '0');
INSERT INTO `ums_menu` VALUES ('25', '21', '2020-02-07 16:31:13', '资源列表', '1', '0', 'resource', 'ums-resource', '0');

-- ----------------------------
-- Table structure for ums_resource
-- ----------------------------
DROP TABLE IF EXISTS `ums_resource`;
CREATE TABLE `ums_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `name` varchar(200) DEFAULT NULL COMMENT '资源名称',
  `url` varchar(200) DEFAULT NULL COMMENT '资源URL',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `category_id` bigint(20) DEFAULT NULL COMMENT '资源分类ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='后台资源表';

-- ----------------------------
-- Records of ums_resource
-- ----------------------------
INSERT INTO `ums_resource` VALUES ('1', '2020-02-04 17:04:55', '商品品牌管理', '/brand/**', null, '1');
INSERT INTO `ums_resource` VALUES ('2', '2020-02-04 17:05:35', '商品属性分类管理', '/productAttribute/**', null, '1');
INSERT INTO `ums_resource` VALUES ('3', '2020-02-04 17:06:13', '商品属性管理', '/productAttribute/**', null, '1');
INSERT INTO `ums_resource` VALUES ('4', '2020-02-04 17:07:15', '商品分类管理', '/productCategory/**', null, '1');
INSERT INTO `ums_resource` VALUES ('5', '2020-02-04 17:09:16', '商品管理', '/product/**', null, '1');
INSERT INTO `ums_resource` VALUES ('6', '2020-02-04 17:09:53', '商品库存管理', '/sku/**', null, '1');
INSERT INTO `ums_resource` VALUES ('8', '2020-02-05 14:43:37', '订单管理', '/order/**', '', '2');
INSERT INTO `ums_resource` VALUES ('9', '2020-02-05 14:44:22', ' 订单退货申请管理', '/returnApply/**', '', '2');
INSERT INTO `ums_resource` VALUES ('10', '2020-02-05 14:45:08', '退货原因管理', '/returnReason/**', '', '2');
INSERT INTO `ums_resource` VALUES ('11', '2020-02-05 14:45:43', '订单设置管理', '/orderSetting/**', '', '2');
INSERT INTO `ums_resource` VALUES ('12', '2020-02-05 14:46:23', '收货地址管理', '/companyAddress/**', '', '2');
INSERT INTO `ums_resource` VALUES ('13', '2020-02-07 16:37:22', '优惠券管理', '/coupon/**', '', '3');
INSERT INTO `ums_resource` VALUES ('14', '2020-02-07 16:37:59', '优惠券领取记录管理', '/couponHistory/**', '', '3');
INSERT INTO `ums_resource` VALUES ('15', '2020-02-07 16:38:28', '限时购活动管理', '/flash/**', '', '3');
INSERT INTO `ums_resource` VALUES ('16', '2020-02-07 16:38:59', '限时购商品关系管理', '/flashProductRelation/**', '', '3');
INSERT INTO `ums_resource` VALUES ('17', '2020-02-07 16:39:22', '限时购场次管理', '/flashSession/**', '', '3');
INSERT INTO `ums_resource` VALUES ('18', '2020-02-07 16:40:07', '首页轮播广告管理', '/home/advertise/**', '', '3');
INSERT INTO `ums_resource` VALUES ('19', '2020-02-07 16:40:34', '首页品牌管理', '/home/brand/**', '', '3');
INSERT INTO `ums_resource` VALUES ('20', '2020-02-07 16:41:06', '首页新品管理', '/home/newProduct/**', '', '3');
INSERT INTO `ums_resource` VALUES ('21', '2020-02-07 16:42:16', '首页人气推荐管理', '/home/recommendProduct/**', '', '3');
INSERT INTO `ums_resource` VALUES ('22', '2020-02-07 16:42:48', '首页专题推荐管理', '/home/recommendSubject/**', '', '3');
INSERT INTO `ums_resource` VALUES ('23', '2020-02-07 16:44:56', ' 商品优选管理', '/prefrenceArea/**', '', '5');
INSERT INTO `ums_resource` VALUES ('24', '2020-02-07 16:45:39', '商品专题管理', '/subject/**', '', '5');
INSERT INTO `ums_resource` VALUES ('25', '2020-02-07 16:47:34', '后台用户管理', '/admin/**', '', '4');
INSERT INTO `ums_resource` VALUES ('26', '2020-02-07 16:48:24', '后台用户角色管理', '/role/**', '', '4');
INSERT INTO `ums_resource` VALUES ('27', '2020-02-07 16:48:48', '后台菜单管理', '/menu/**', '', '4');
INSERT INTO `ums_resource` VALUES ('28', '2020-02-07 16:49:18', '后台资源分类管理', '/resourceCategory/**', '', '4');
INSERT INTO `ums_resource` VALUES ('29', '2020-02-07 16:49:45', '后台资源管理', '/resource/**', '', '4');

-- ----------------------------
-- Table structure for ums_resource_category
-- ----------------------------
DROP TABLE IF EXISTS `ums_resource_category`;
CREATE TABLE `ums_resource_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `name` varchar(200) DEFAULT NULL COMMENT '分类名称',
  `sort` int(4) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='资源分类表';

-- ----------------------------
-- Records of ums_resource_category
-- ----------------------------
INSERT INTO `ums_resource_category` VALUES ('1', '2020-02-05 10:21:44', '商品模块', '0');
INSERT INTO `ums_resource_category` VALUES ('2', '2020-02-05 10:22:34', '订单模块', '0');
INSERT INTO `ums_resource_category` VALUES ('3', '2020-02-05 10:22:48', '营销模块', '0');
INSERT INTO `ums_resource_category` VALUES ('4', '2020-02-05 10:23:04', '权限模块', '0');
INSERT INTO `ums_resource_category` VALUES ('5', '2020-02-07 16:34:27', '内容模块', '0');
INSERT INTO `ums_resource_category` VALUES ('6', '2020-02-07 16:35:49', '其他模块', '0');

-- ----------------------------
-- Table structure for ums_role
-- ----------------------------
DROP TABLE IF EXISTS `ums_role`;
CREATE TABLE `ums_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `admin_count` int(11) DEFAULT NULL COMMENT '后台用户数量',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` int(1) DEFAULT '1' COMMENT '启用状态：0->禁用；1->启用',
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='后台用户角色表';

-- ----------------------------
-- Records of ums_role
-- ----------------------------
INSERT INTO `ums_role` VALUES ('1', '商品管理员', '只能查看及操作商品', '0', '2020-02-03 16:50:37', '1', '0');
INSERT INTO `ums_role` VALUES ('2', '订单管理员', '只能查看及操作订单', '0', '2018-09-30 15:53:45', '1', '0');
INSERT INTO `ums_role` VALUES ('5', '超级管理员', '拥有所有查看和操作功能', '0', '2020-02-02 15:11:05', '1', '0');
INSERT INTO `ums_role` VALUES ('8', '权限管理员', '用于权限模块所有操作功能', '0', '2020-08-24 10:57:35', '1', '0');

-- ----------------------------
-- Table structure for ums_role_menu_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_role_menu_relation`;
CREATE TABLE `ums_role_menu_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COMMENT='后台角色菜单关系表';

-- ----------------------------
-- Records of ums_role_menu_relation
-- ----------------------------
INSERT INTO `ums_role_menu_relation` VALUES ('33', '1', '1');
INSERT INTO `ums_role_menu_relation` VALUES ('34', '1', '2');
INSERT INTO `ums_role_menu_relation` VALUES ('35', '1', '3');
INSERT INTO `ums_role_menu_relation` VALUES ('36', '1', '4');
INSERT INTO `ums_role_menu_relation` VALUES ('37', '1', '5');
INSERT INTO `ums_role_menu_relation` VALUES ('38', '1', '6');
INSERT INTO `ums_role_menu_relation` VALUES ('53', '2', '7');
INSERT INTO `ums_role_menu_relation` VALUES ('54', '2', '8');
INSERT INTO `ums_role_menu_relation` VALUES ('55', '2', '9');
INSERT INTO `ums_role_menu_relation` VALUES ('56', '2', '10');
INSERT INTO `ums_role_menu_relation` VALUES ('57', '2', '11');
INSERT INTO `ums_role_menu_relation` VALUES ('72', '5', '1');
INSERT INTO `ums_role_menu_relation` VALUES ('73', '5', '2');
INSERT INTO `ums_role_menu_relation` VALUES ('74', '5', '3');
INSERT INTO `ums_role_menu_relation` VALUES ('75', '5', '4');
INSERT INTO `ums_role_menu_relation` VALUES ('76', '5', '5');
INSERT INTO `ums_role_menu_relation` VALUES ('77', '5', '6');
INSERT INTO `ums_role_menu_relation` VALUES ('78', '5', '7');
INSERT INTO `ums_role_menu_relation` VALUES ('79', '5', '8');
INSERT INTO `ums_role_menu_relation` VALUES ('80', '5', '9');
INSERT INTO `ums_role_menu_relation` VALUES ('81', '5', '10');
INSERT INTO `ums_role_menu_relation` VALUES ('82', '5', '11');
INSERT INTO `ums_role_menu_relation` VALUES ('83', '5', '12');
INSERT INTO `ums_role_menu_relation` VALUES ('84', '5', '13');
INSERT INTO `ums_role_menu_relation` VALUES ('85', '5', '14');
INSERT INTO `ums_role_menu_relation` VALUES ('86', '5', '16');
INSERT INTO `ums_role_menu_relation` VALUES ('87', '5', '17');
INSERT INTO `ums_role_menu_relation` VALUES ('88', '5', '18');
INSERT INTO `ums_role_menu_relation` VALUES ('89', '5', '19');
INSERT INTO `ums_role_menu_relation` VALUES ('90', '5', '20');
INSERT INTO `ums_role_menu_relation` VALUES ('91', '5', '21');
INSERT INTO `ums_role_menu_relation` VALUES ('92', '5', '22');
INSERT INTO `ums_role_menu_relation` VALUES ('93', '5', '23');
INSERT INTO `ums_role_menu_relation` VALUES ('94', '5', '24');
INSERT INTO `ums_role_menu_relation` VALUES ('95', '5', '25');
INSERT INTO `ums_role_menu_relation` VALUES ('96', '6', '21');
INSERT INTO `ums_role_menu_relation` VALUES ('97', '6', '22');
INSERT INTO `ums_role_menu_relation` VALUES ('98', '6', '23');
INSERT INTO `ums_role_menu_relation` VALUES ('99', '6', '24');
INSERT INTO `ums_role_menu_relation` VALUES ('100', '6', '25');
INSERT INTO `ums_role_menu_relation` VALUES ('101', '7', '21');
INSERT INTO `ums_role_menu_relation` VALUES ('102', '7', '22');
INSERT INTO `ums_role_menu_relation` VALUES ('103', '7', '23');
INSERT INTO `ums_role_menu_relation` VALUES ('104', '7', '24');
INSERT INTO `ums_role_menu_relation` VALUES ('105', '7', '25');
INSERT INTO `ums_role_menu_relation` VALUES ('106', '8', '21');
INSERT INTO `ums_role_menu_relation` VALUES ('107', '8', '22');
INSERT INTO `ums_role_menu_relation` VALUES ('108', '8', '23');
INSERT INTO `ums_role_menu_relation` VALUES ('109', '8', '24');
INSERT INTO `ums_role_menu_relation` VALUES ('110', '8', '25');

-- ----------------------------
-- Table structure for ums_role_resource_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_role_resource_relation`;
CREATE TABLE `ums_role_resource_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `resource_id` bigint(20) DEFAULT NULL COMMENT '资源ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8 COMMENT='后台角色资源关系表';

-- ----------------------------
-- Records of ums_role_resource_relation
-- ----------------------------
INSERT INTO `ums_role_resource_relation` VALUES ('103', '2', '8');
INSERT INTO `ums_role_resource_relation` VALUES ('104', '2', '9');
INSERT INTO `ums_role_resource_relation` VALUES ('105', '2', '10');
INSERT INTO `ums_role_resource_relation` VALUES ('106', '2', '11');
INSERT INTO `ums_role_resource_relation` VALUES ('107', '2', '12');
INSERT INTO `ums_role_resource_relation` VALUES ('142', '5', '1');
INSERT INTO `ums_role_resource_relation` VALUES ('143', '5', '2');
INSERT INTO `ums_role_resource_relation` VALUES ('144', '5', '3');
INSERT INTO `ums_role_resource_relation` VALUES ('145', '5', '4');
INSERT INTO `ums_role_resource_relation` VALUES ('146', '5', '5');
INSERT INTO `ums_role_resource_relation` VALUES ('147', '5', '6');
INSERT INTO `ums_role_resource_relation` VALUES ('148', '5', '8');
INSERT INTO `ums_role_resource_relation` VALUES ('149', '5', '9');
INSERT INTO `ums_role_resource_relation` VALUES ('150', '5', '10');
INSERT INTO `ums_role_resource_relation` VALUES ('151', '5', '11');
INSERT INTO `ums_role_resource_relation` VALUES ('152', '5', '12');
INSERT INTO `ums_role_resource_relation` VALUES ('153', '5', '13');
INSERT INTO `ums_role_resource_relation` VALUES ('154', '5', '14');
INSERT INTO `ums_role_resource_relation` VALUES ('155', '5', '15');
INSERT INTO `ums_role_resource_relation` VALUES ('156', '5', '16');
INSERT INTO `ums_role_resource_relation` VALUES ('157', '5', '17');
INSERT INTO `ums_role_resource_relation` VALUES ('158', '5', '18');
INSERT INTO `ums_role_resource_relation` VALUES ('159', '5', '19');
INSERT INTO `ums_role_resource_relation` VALUES ('160', '5', '20');
INSERT INTO `ums_role_resource_relation` VALUES ('161', '5', '21');
INSERT INTO `ums_role_resource_relation` VALUES ('162', '5', '22');
INSERT INTO `ums_role_resource_relation` VALUES ('163', '5', '23');
INSERT INTO `ums_role_resource_relation` VALUES ('164', '5', '24');
INSERT INTO `ums_role_resource_relation` VALUES ('165', '5', '25');
INSERT INTO `ums_role_resource_relation` VALUES ('166', '5', '26');
INSERT INTO `ums_role_resource_relation` VALUES ('167', '5', '27');
INSERT INTO `ums_role_resource_relation` VALUES ('168', '5', '28');
INSERT INTO `ums_role_resource_relation` VALUES ('169', '5', '29');
INSERT INTO `ums_role_resource_relation` VALUES ('170', '1', '1');
INSERT INTO `ums_role_resource_relation` VALUES ('171', '1', '2');
INSERT INTO `ums_role_resource_relation` VALUES ('172', '1', '3');
INSERT INTO `ums_role_resource_relation` VALUES ('173', '1', '4');
INSERT INTO `ums_role_resource_relation` VALUES ('174', '1', '5');
INSERT INTO `ums_role_resource_relation` VALUES ('175', '1', '6');
INSERT INTO `ums_role_resource_relation` VALUES ('176', '1', '23');
INSERT INTO `ums_role_resource_relation` VALUES ('177', '1', '24');
INSERT INTO `ums_role_resource_relation` VALUES ('178', '6', '25');
INSERT INTO `ums_role_resource_relation` VALUES ('179', '6', '26');
INSERT INTO `ums_role_resource_relation` VALUES ('180', '6', '27');
INSERT INTO `ums_role_resource_relation` VALUES ('181', '6', '28');
INSERT INTO `ums_role_resource_relation` VALUES ('182', '6', '29');
INSERT INTO `ums_role_resource_relation` VALUES ('205', '7', '25');
INSERT INTO `ums_role_resource_relation` VALUES ('206', '7', '26');
INSERT INTO `ums_role_resource_relation` VALUES ('207', '7', '27');
INSERT INTO `ums_role_resource_relation` VALUES ('208', '7', '28');
INSERT INTO `ums_role_resource_relation` VALUES ('209', '7', '29');
INSERT INTO `ums_role_resource_relation` VALUES ('210', '7', '31');
INSERT INTO `ums_role_resource_relation` VALUES ('211', '8', '25');
INSERT INTO `ums_role_resource_relation` VALUES ('212', '8', '26');
INSERT INTO `ums_role_resource_relation` VALUES ('213', '8', '27');
INSERT INTO `ums_role_resource_relation` VALUES ('214', '8', '28');
INSERT INTO `ums_role_resource_relation` VALUES ('215', '8', '29');


-- ----------------------------
-- Table structure for card_binding
-- ----------------------------
DROP TABLE IF EXISTS `card_binding`;
CREATE TABLE `card_binding`  (
                                 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                 `card_number` int(11) NOT NULL DEFAULT 0 COMMENT '卡片序列号',
                                 `customer_id` int(11) NOT NULL DEFAULT 0 COMMENT '集团客户id',
                                 `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '绑定会员id',
                                 `binding_time` timestamp NULL DEFAULT NULL,
                                 `face_value` int(11) NOT NULL DEFAULT 0 COMMENT '面额(kg)',
                                 `balance` decimal(9, 2) NOT NULL DEFAULT 0.00,
                                 `status` tinyint(2) NOT NULL DEFAULT 10 COMMENT '状态 10未绑定 20已绑定 30取消绑定',
                                 `service_number` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '银行消费券号',
                                 `create_at` datetime NOT NULL,
                                 `update_at` datetime NOT NULL,
                                 `delete_at` datetime NULL DEFAULT NULL,
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 901 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '卡片绑定管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of card_binding
-- ----------------------------
INSERT INTO `card_binding` VALUES (781, 10001, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (782, 10002, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (783, 10003, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (784, 10004, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (785, 10005, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (786, 10006, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (787, 10007, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (788, 10008, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (789, 10009, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (790, 10010, 1, 1001, NULL, 1000, 1000.00, 20, '156456465', '2023-04-07 17:00:25', '2023-04-07 17:00:25', NULL);
INSERT INTO `card_binding` VALUES (891, 10111, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);
INSERT INTO `card_binding` VALUES (892, 10112, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);
INSERT INTO `card_binding` VALUES (893, 10113, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);
INSERT INTO `card_binding` VALUES (894, 10114, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);
INSERT INTO `card_binding` VALUES (895, 10115, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);
INSERT INTO `card_binding` VALUES (896, 10116, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);
INSERT INTO `card_binding` VALUES (897, 10117, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);
INSERT INTO `card_binding` VALUES (898, 10118, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);
INSERT INTO `card_binding` VALUES (899, 10119, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);
INSERT INTO `card_binding` VALUES (900, 10120, 1, 1001, NULL, 1000, 1000.00, 20, '456465', '2023-07-03 15:21:38', '2023-07-03 15:21:38', NULL);

-- ----------------------------
-- Table structure for card_publish
-- ----------------------------
DROP TABLE IF EXISTS `card_publish`;
CREATE TABLE `card_publish`  (
                                 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                 `publish_time` timestamp NULL DEFAULT NULL,
                                 `admin_id` int(11) NOT NULL DEFAULT 0 COMMENT '操作人id',
                                 `customer_id` int(11) NOT NULL DEFAULT 0 COMMENT '集团客户id',
                                 `skin_id` int(11) NOT NULL DEFAULT 0 COMMENT '卡面编号id',
                                 `count` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
                                 `begin_number` int(11) NOT NULL DEFAULT 0 COMMENT '起始卡片序列号',
                                 `end_number` int(11) NOT NULL DEFAULT 0 COMMENT '截止卡片序列号',
                                 `status` tinyint(2) NOT NULL DEFAULT 10 COMMENT '状态 10添加库存 20确认发行 30取消发行 40批次停用',
                                 `type` tinyint(2) NOT NULL DEFAULT 10 COMMENT '类型 10预存费用型 20用户充值型',
                                 `service_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
                                 `service_order_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
                                 `service_money` decimal(9, 2) NOT NULL DEFAULT 0.00,
                                 `create_at` datetime NOT NULL,
                                 `update_at` datetime NOT NULL,
                                 `delete_at` datetime NULL DEFAULT NULL,
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '卡片发行记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of card_publish
-- ----------------------------
INSERT INTO `card_publish` VALUES (5, '2023-04-07 17:00:25', 2, 1, 1, 10, 10001, 10010, 40, 10, '156456465', '202304071603386681300648', 45.25, '2023-04-07 16:03:38', '2023-04-07 17:05:03', NULL);
INSERT INTO `card_publish` VALUES (6, '2023-07-03 14:40:10', 2, 1, 1, 100, 10011, 10110, 40, 10, '156465', '202307031439208215433669', 45.25, '2023-07-03 14:39:20', '2023-07-03 14:42:50', NULL);
INSERT INTO `card_publish` VALUES (7, '2023-07-03 15:21:38', 2, 1, 1, 10, 10111, 10120, 40, 10, '456465', '202307031520414961997605', 45.25, '2023-07-03 15:20:41', '2023-07-03 15:30:46', NULL);

-- ----------------------------
-- Table structure for card_skin
-- ----------------------------
DROP TABLE IF EXISTS `card_skin`;
CREATE TABLE `card_skin`  (
                              `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
                              `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
                              `face_value` int(11) NOT NULL DEFAULT 0 COMMENT '面额(kg)',
                              `colour` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '颜色',
                              `customer_id` int(11) NOT NULL DEFAULT 0 COMMENT '集团客户id',
                              `type` tinyint(2) NOT NULL DEFAULT 10 COMMENT '类型 10实体卡片 20数字卡密',
                              `status` tinyint(2) NOT NULL DEFAULT 10 COMMENT '状态 10启用 20停用',
                              `image` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '略缩图',
                              `admin_id` int(11) NOT NULL DEFAULT 0 COMMENT '创建人id',
                              `create_at` datetime NOT NULL,
                              `update_at` datetime NOT NULL,
                              `delete_at` datetime NULL DEFAULT NULL,
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '卡面管理(皮肤)' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of card_skin
-- ----------------------------
INSERT INTO `card_skin` VALUES (1, '海绵宝宝限定皮肤1', 1000, '#000000', 12, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:21:45', '2023-03-31 14:21:45', NULL);
INSERT INTO `card_skin` VALUES (2, '海绵宝宝限定皮肤', 2000, '#000000', 12, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:22:00', '2023-03-31 14:23:10', NULL);
INSERT INTO `card_skin` VALUES (3, '海绵宝宝限定皮肤', 1000, '#000000', 18, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:24:35', '2023-03-31 17:46:33', NULL);
INSERT INTO `card_skin` VALUES (4, '海绵宝宝限定皮肤', 2000, '#000000', 18, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 9, '2023-04-07 15:37:48', '2023-04-07 15:38:29', NULL);
INSERT INTO `card_skin` VALUES (5, '海绵宝宝限定皮肤', 1000, '#000000', 18, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:21:45', '2023-03-31 14:21:45', NULL);
INSERT INTO `card_skin` VALUES (6, '海绵宝宝限定皮肤', 1000, '#000000', 12, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:21:45', '2023-03-31 14:21:45', NULL);
INSERT INTO `card_skin` VALUES (9, '海绵宝宝限定皮肤1', 1009, '#000099', 17, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:21:45', '2023-03-31 14:21:45', NULL);
INSERT INTO `card_skin` VALUES (10, '海绵宝宝限定皮肤01', 1000, '#000000', 40, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:21:45', '2023-03-31 14:21:45', NULL);
INSERT INTO `card_skin` VALUES (11, '海绵宝宝限定皮肤02', 2000, '#000000', 40, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:22:00', '2023-03-31 14:23:10', NULL);
INSERT INTO `card_skin` VALUES (12, '海绵宝宝限定皮肤03', 1000, '#000000', 40, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:24:35', '2023-03-31 17:46:33', NULL);
INSERT INTO `card_skin` VALUES (13, '海绵宝宝限定皮肤01', 1000, '#000000', 41, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:21:45', '2023-03-31 14:21:45', NULL);
INSERT INTO `card_skin` VALUES (14, '海绵宝宝限定皮肤02', 2000, '#000000', 41, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:22:00', '2023-03-31 14:23:10', NULL);
INSERT INTO `card_skin` VALUES (15, '海绵宝宝限定皮肤03', 1000, '#000000', 41, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:24:35', '2023-03-31 17:46:33', NULL);
INSERT INTO `card_skin` VALUES (16, '海绵宝宝限定皮肤01', 1000, '#000000', 41, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:21:45', '2023-03-31 14:21:45', NULL);
INSERT INTO `card_skin` VALUES (17, '海绵宝宝限定皮肤02', 2000, '#000000', 41, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-03-31 14:22:00', '2023-03-31 14:23:10', NULL);
INSERT INTO `card_skin` VALUES (20, '海绵宝宝限定皮肤1', 1000, '#000000', 12, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-07-18 10:40:59', '2023-07-18 10:40:59', NULL);
INSERT INTO `card_skin` VALUES (21, '海绵11', 1001, '#000000', 12, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-07-18 11:34:00', '2023-07-18 11:34:00', NULL);
INSERT INTO `card_skin` VALUES (22, '海绵11', 1001, '#000000', 12, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-07-18 11:37:19', '2023-07-18 11:37:19', NULL);
INSERT INTO `card_skin` VALUES (23, '海绵11', 1001, '#000000', 12, 10, 10, 'https://learnku.com/docs/iris-go/10/sessions/3779', 2, '2023-07-18 13:45:30', '2023-07-18 13:45:30', NULL);

-- ----------------------------
-- Table structure for card_traceability
-- ----------------------------
DROP TABLE IF EXISTS `card_traceability`;
CREATE TABLE `card_traceability`  (
                                      `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                      `card_number` int(11) NOT NULL DEFAULT 0 COMMENT '卡片序列号',
                                      `storage_admin_id` int(11) NOT NULL DEFAULT 0 COMMENT '入库操作人id',
                                      `storage_time` timestamp NULL DEFAULT NULL,
                                      `publish_admin_id` int(11) NOT NULL DEFAULT 0 COMMENT '发行操作人id',
                                      `publish_time` timestamp NULL DEFAULT NULL,
                                      `receive_admin_id` int(11) NOT NULL DEFAULT 0 COMMENT '领用销售员id',
                                      `receive_time` timestamp NULL DEFAULT NULL,
                                      `binding_user_id` int(11) NOT NULL DEFAULT 0 COMMENT '绑定会员id',
                                      `binding_time` timestamp NULL DEFAULT NULL,
                                      `write_off_time` timestamp NULL DEFAULT NULL,
                                      `status` tinyint(2) NOT NULL DEFAULT 10 COMMENT '状态 10入库 20发行 30领用 40绑定 50使用 60销毁',
                                      `create_at` datetime NOT NULL,
                                      `update_at` datetime NOT NULL,
                                      `delete_at` datetime NULL DEFAULT NULL,
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '卡片溯源记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of card_traceability
-- ----------------------------

-- ----------------------------
-- Table structure for sys_customer
-- ----------------------------
DROP TABLE IF EXISTS `sys_customer`;
CREATE TABLE `sys_customer`  (
                                 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                 `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系人',
                                 `mobile_phone` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系电话手机',
                                 `landline_phone` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系电话座机',
                                 `area` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '区域（省-市）',
                                 `address` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系地址',
                                 `email` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
                                 `begin_times` timestamp NULL DEFAULT NULL COMMENT '合作起始时间',
                                 `end_times` timestamp NULL DEFAULT NULL COMMENT '合作终止时间',
                                 `status` tinyint(2) NOT NULL DEFAULT 10 COMMENT '合作状态 10有效 20终止',
                                 `create_at` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                 `update_at` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
                                 `delete_at` datetime NULL DEFAULT NULL COMMENT '删除时间',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '集团客户' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_customer
-- ----------------------------
INSERT INTO `sys_customer` VALUES (12, '吴亦凡', '13983327099', '74587126', '重庆市江北区', '昭阳路127号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-30 15:33:35', '2023-04-06 17:59:25', NULL);
INSERT INTO `sys_customer` VALUES (14, '李四', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-31 11:31:26', '2023-04-06 17:59:34', NULL);
INSERT INTO `sys_customer` VALUES (15, '王麻子', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-31 11:35:58', '2023-03-31 11:36:12', NULL);
INSERT INTO `sys_customer` VALUES (16, '王五', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-31 11:38:09', '2023-03-31 11:38:27', NULL);
INSERT INTO `sys_customer` VALUES (17, '刘三', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', 'A1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-31 11:39:24', '2023-03-31 11:40:10', NULL);
INSERT INTO `sys_customer` VALUES (18, '集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-04-06 17:14:07', '2023-04-06 17:14:09', NULL);
INSERT INTO `sys_customer` VALUES (19, '修改后的集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-04-06 17:23:34', '2023-04-06 17:27:24', NULL);
INSERT INTO `sys_customer` VALUES (20, '修改后的集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-04-07 14:58:43', '2023-04-07 15:09:15', NULL);
INSERT INTO `sys_customer` VALUES (21, '修改后的集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-04-07 15:06:34', '2023-04-07 15:09:15', NULL);
INSERT INTO `sys_customer` VALUES (22, '修改后的集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-04-07 15:06:34', '2023-04-07 15:09:15', NULL);
INSERT INTO `sys_customer` VALUES (23, '修改后的集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-04-07 15:06:34', '2023-04-07 15:09:15', NULL);
INSERT INTO `sys_customer` VALUES (27, '集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-30 15:33:35', '2023-04-06 17:59:25', NULL);
INSERT INTO `sys_customer` VALUES (28, '集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-30 15:33:35', '2023-04-06 17:59:25', NULL);
INSERT INTO `sys_customer` VALUES (29, '集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-30 15:33:35', '2023-04-06 17:59:25', NULL);
INSERT INTO `sys_customer` VALUES (40, '集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-30 15:33:35', '2023-04-06 17:59:25', NULL);
INSERT INTO `sys_customer` VALUES (41, '集团名称', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-03-30 15:33:35', '2023-04-06 17:59:25', NULL);
INSERT INTO `sys_customer` VALUES (42, '张三', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-07-14 11:46:47', '2023-07-14 11:46:47', NULL);
INSERT INTO `sys_customer` VALUES (43, '张三', '13983327099', '74587126', '重庆市江北区', '昭阳路128号', '1301969706@qq.com', '2023-03-29 00:00:00', '2023-04-29 00:00:00', 10, '2023-07-14 11:47:26', '2023-07-14 11:47:26', NULL);

SET FOREIGN_KEY_CHECKS = 1;
