package com.macro.mall.tiny.modules.sys.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 集团客户
 * </p>
 *
 * @author macro
 * @since 2023-07-12
 */
@Getter
@Setter
@TableName("sys_customer")
@ApiModel(value = "SysCustomer对象", description = "集团客户")
public class SysCustomer implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("联系人")
    private String name;

    @ApiModelProperty("联系电话手机")
    private String mobilePhone;

    @ApiModelProperty("联系电话座机")
    private String landlinePhone;

    @ApiModelProperty("区域（省-市）")
    private String area;

    @ApiModelProperty("联系地址")
    private String address;

    @ApiModelProperty("电子邮箱")
    private String email;

    @ApiModelProperty("合作起始时间")
    private Date beginTimes;

    @ApiModelProperty("合作终止时间")
    private Date endTimes;

    @ApiModelProperty("合作状态 10有效 20终止")
    private Integer status;

    @ApiModelProperty("创建时间")
    private Date createAt;

    @ApiModelProperty("更新时间")
    private Date updateAt;

    @ApiModelProperty("删除时间")
    private Date deleteAt;


}
